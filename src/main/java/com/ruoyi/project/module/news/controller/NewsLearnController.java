package com.ruoyi.project.module.news.controller;

import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.utils.security.ShiroUtils;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.module.news.domain.News;
import com.ruoyi.project.module.news.service.INewsService;
import com.ruoyi.project.module.type.domain.NewsType;
import com.ruoyi.project.module.type.service.INewsTypeService;
import com.ruoyi.project.module.upload.service.INewsUploadService;
import com.ruoyi.project.system.dept.domain.Dept;
import com.ruoyi.project.system.dept.service.IDeptService;
import com.ruoyi.project.system.notice.domain.Notice;
import com.ruoyi.project.system.notice.service.INoticeService;
import com.ruoyi.project.system.user.domain.User;
import com.ruoyi.project.system.user.service.IUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 资讯Controller
 *
 * @author ruoyi
 * @date 2020-05-06
 */
@Controller
@RequestMapping("/module/newsLearn")
public class NewsLearnController extends BaseController {
    private String prefix = "module/newsLearn";

    @Autowired
    private INewsService newsService;
    @Autowired
    private INewsTypeService newsTypeService;
    @Autowired
    private IUserService userService;
    @Autowired
    private INoticeService noticeService;
    @Autowired
    private IDeptService deptService;
    @Autowired
    private INewsUploadService uploadService;

    @RequiresPermissions("module:news:view")
    @GetMapping()
    public String news(ModelMap mmap) {
        //所有分类
        NewsType type = new NewsType();
        type.setType("2"); //在线学习分类
        List<NewsType> typeList = newsTypeService.selectNewsTypeList(type);

        //所有部门
        List<Dept> deptList = deptService.selectDeptList(new Dept());
        mmap.put("typeList", typeList);
        mmap.put("deptList", deptList);
        return prefix + "/newsLearn";
    }

    /**
     * 查询资讯列表
     */
    @RequiresPermissions("module:news:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(News news) {
        startPage();
        List<News> list = newsService.selectNewsList(news);
        return getDataTable(list);
    }

    /**
     * 导出资讯列表
     */
    @RequiresPermissions("module:news:export")
    @Log(title = "资讯", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(News news) {
        List<News> list = newsService.selectNewsList(news);
        ExcelUtil<News> util = new ExcelUtil<News>(News.class);
        return util.exportExcel(list, "news");
    }

    /**
     * 新增资讯
     */
    @GetMapping("/add")
    public String add(ModelMap mmap) {
        //在线学习分类
        NewsType type = new NewsType();
        type.setType("2"); //
        List<NewsType> typeList = newsTypeService.selectNewsTypeList(type);
        mmap.put("typeList", typeList);

        //上传时间
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time= dateFormat.format(new Date());
        mmap.put("createTime",time);

        //默认管理员审核
        User user = userService.selectUserById((long) 1);
        mmap.put("userId",1);
        mmap.put("userName",user.getUserName());
        return prefix + "/add";
    }

    /**
     * 新增保存资讯
     */
    @RequiresPermissions("module:news:add")
    @Log(title = "资讯", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(News news) {
        return toAjax(newsService.insertNews(news));
    }

    /**
     * 修改资讯
     */
    @GetMapping("/edit/{newId}")
    public String edit(@PathVariable("newId") Long newId, ModelMap mmap) {
        News news = newsService.selectNewsById(newId);
        mmap.put("news", news);
//        if(news.getNewFile()!=null ){
//            String[] files = news.getNewFile().split(",");
//            mmap.put("files",files);
//        }

        //在线学习分类
        NewsType type = new NewsType();
        type.setType("2"); //
        List<NewsType> typeList = newsTypeService.selectNewsTypeList(type);
        mmap.put("typeList", typeList);

        return prefix + "/edit";
    }

    /**
     * 修改保存资讯
     */
    @RequiresPermissions("module:news:edit")
    @Log(title = "资讯", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(News news) {
        return toAjax(newsService.updateNews(news));
    }

    /**
     * 删除资讯
     */
    @RequiresPermissions("module:news:remove")
    @Log(title = "资讯", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(newsService.deleteNewsByIds(ids));
    }


    /**
     * 我的审批
     *
     * @return
     */
    @RequiresPermissions("module:news:view")
    @GetMapping("/approval")
    public String approvalNews() {
        return prefix + "/approvalNews";
    }

    /**
     * 资讯审批
     *
     * @param news
     * @return
     */
    @RequiresPermissions("module:news:list")
    @PostMapping("/approvalList")
    @ResponseBody
    public TableDataInfo approvalList(News news) {
        startPage();
        news.setType("2"); //在线学习审核
        news.setUserId(ShiroUtils.getSysUser().getUserId());
        List<News> list = newsService.selectNewsList(news);
        return getDataTable(list);
    }

    /**
     * 公告审批
     */

    @RequiresPermissions("module:news:list")
    @PostMapping("/noticeList")
    @ResponseBody
    public TableDataInfo noticeList(Notice notice) {
        startPage();
        notice.setUserId(ShiroUtils.getSysUser().getUserId());
        List<Notice> list = noticeService.selectNoticeList(notice);
        return getDataTable(list);
    }

    /**
     * 上传
     */
    @GetMapping("/upload")
    public String upload(News news,ModelMap mmap){
       mmap.put("newId",news.getNewId());
       return prefix+"/newUpload";
    }


    /**
     * 查看详情
     * @param newId
     * @param mmap
     * @return
     */
    @GetMapping("/detail/{newId}")
    public String detail(@PathVariable("newId") Long newId, ModelMap mmap) {
        News news = newsService.selectNewsById(newId);
        mmap.put("news", news);


        if(news.getNewVideo()!=null){
            String[] videos = news.getNewVideo().split(",");
            mmap.put("videos",videos);
        }

        return prefix + "/detail";
    }

}
