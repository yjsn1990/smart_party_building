package com.ruoyi.project.module.feel.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 心得体会对象 tb_feel
 * 
 * @author ruoyi
 * @date 2020-05-09
 */
public class Feel extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long feelId;

    /** 部门id */
    @Excel(name = "部门id")
    private Long deptId;

    private String deptName;

    /** 提交人id */
    @Excel(name = "提交人id")
    private Long userId;

    /** 提交人姓名 */
    @Excel(name = "提交人姓名")
    private String userName;

    /** 提交时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date feelTime;

    /** 标题 */
    @Excel(name = "标题")
    private String feelTitle;

    /** 内容 */
    @Excel(name = "内容")
    private String feelContent;

    /** 图片 */
    @Excel(name = "图片")
    private String feelImg;

    /** 视频 */
    @Excel(name = "视频")
    private String feelVideo;

    /** 状态（0正常 1关闭） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=关闭")
    private String status;

    public void setFeelId(Long feelId) 
    {
        this.feelId = feelId;
    }

    public Long getFeelId() 
    {
        return feelId;
    }
    public void setDeptId(Long deptId) 
    {
        this.deptId = deptId;
    }

    public Long getDeptId() 
    {
        return deptId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setFeelTime(Date feelTime) 
    {
        this.feelTime = feelTime;
    }

    public Date getFeelTime() 
    {
        return feelTime;
    }
    public void setFeelTitle(String feelTitle) 
    {
        this.feelTitle = feelTitle;
    }

    public String getFeelTitle() 
    {
        return feelTitle;
    }
    public void setFeelContent(String feelContent) 
    {
        this.feelContent = feelContent;
    }

    public String getFeelContent() 
    {
        return feelContent;
    }
    public void setFeelImg(String feelImg) 
    {
        this.feelImg = feelImg;
    }

    public String getFeelImg() 
    {
        return feelImg;
    }
    public void setFeelVideo(String feelVideo) 
    {
        this.feelVideo = feelVideo;
    }

    public String getFeelVideo() 
    {
        return feelVideo;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("feelId", getFeelId())
            .append("deptId", getDeptId())
            .append("userId", getUserId())
            .append("userName", getUserName())
            .append("feelTime", getFeelTime())
            .append("feelTitle", getFeelTitle())
            .append("feelContent", getFeelContent())
            .append("feelImg", getFeelImg())
            .append("feelVideo", getFeelVideo())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
