package com.ruoyi.project.module.feel.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.security.ShiroUtils;
import com.ruoyi.project.system.dept.domain.Dept;
import com.ruoyi.project.system.dept.service.IDeptService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.module.feel.domain.Feel;
import com.ruoyi.project.module.feel.service.IFeelService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 心得体会Controller
 * 
 * @author ruoyi
 * @date 2020-05-09
 */
@Controller
@RequestMapping("/module/feel")
public class FeelController extends BaseController
{
    private String prefix = "module/feel";

    @Autowired
    private IFeelService feelService;
    @Autowired
    private IDeptService deptService;

    @RequiresPermissions("module:feel:view")
    @GetMapping()
    public String feel(ModelMap mmap)
    {
        //所有部门
        List<Dept> deptList = deptService.selectDeptList(new Dept());
        mmap.put("deptList", deptList);
        return prefix + "/feel";
    }

    /**
     * 查询心得体会列表
     */
    @RequiresPermissions("module:feel:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Feel feel)
    {
        startPage();
        List<Feel> list = feelService.selectFeelList(feel);
        return getDataTable(list);
    }

    /**
     * 导出心得体会列表
     */
    @RequiresPermissions("module:feel:export")
    @Log(title = "心得体会", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Feel feel)
    {
        List<Feel> list = feelService.selectFeelList(feel);
        ExcelUtil<Feel> util = new ExcelUtil<Feel>(Feel.class);
        return util.exportExcel(list, "feel");
    }

    /**
     * 新增心得体会
     */
    @GetMapping("/add")
    public String add(ModelMap mmap)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time= dateFormat.format(new Date());
        mmap.put("time",time);
        return prefix + "/add";
    }

    /**
     * 新增保存心得体会
     */
    @RequiresPermissions("module:feel:add")
    @Log(title = "心得体会", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Feel feel)
    {
        feel.setUserId(ShiroUtils.getSysUser().getUserId());
        feel.setFeelTime(DateUtils.getNowDate());
        return toAjax(feelService.insertFeel(feel));
    }

    /**
     * 修改心得体会
     */
    @GetMapping("/edit/{feelId}")
    public String edit(@PathVariable("feelId") Long feelId, ModelMap mmap)
    {
        Feel feel = feelService.selectFeelById(feelId);
        mmap.put("feel", feel);

        if(feel.getFeelVideo()!=null){
            String[] videos = feel.getFeelVideo().split(",");
            mmap.put("videos",videos);
        }

        return prefix + "/edit";
    }

    /**
     * 修改保存心得体会
     */
    @RequiresPermissions("module:feel:edit")
    @Log(title = "心得体会", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Feel feel)
    {
        return toAjax(feelService.updateFeel(feel));
    }

    /**
     * 删除心得体会
     */
    @RequiresPermissions("module:feel:remove")
    @Log(title = "心得体会", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(feelService.deleteFeelByIds(ids));
    }


    @GetMapping("/detail/{feelId}")
    public String detail(@PathVariable("feelId") Long feelId, ModelMap mmap)
    {
        Feel feel = feelService.selectFeelById(feelId);
        mmap.put("feel", feel);

        if(feel.getFeelVideo()!=null){
            String[] videos = feel.getFeelVideo().split(",");
            mmap.put("videos",videos);
        }

        return prefix + "/detail";
    }
}
