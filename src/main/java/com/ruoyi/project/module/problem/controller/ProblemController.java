package com.ruoyi.project.module.problem.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.security.ShiroUtils;
import com.ruoyi.project.module.article.domain.Article;
import com.ruoyi.project.module.deed.service.IDeedService;
import com.ruoyi.project.module.problem.domain.Problem;
import com.ruoyi.project.module.problem.service.IProblemService;
import com.ruoyi.project.system.dept.domain.Dept;
import com.ruoyi.project.system.dept.service.IDeptService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;

import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 问题墙Controller
 * 
 * @author ruoyi
 * @date 2020-05-11
 */
@Controller
@RequestMapping("/module/problem")
public class ProblemController extends BaseController
{
    private String prefix = "module/problem";

    @Autowired
    private IProblemService problemService;
    @Autowired
    private IDeptService deptService;

    @RequiresPermissions("module:problem:view")
    @GetMapping()
    public String problem(ModelMap mmap)
    {
        //所有部门
        List<Dept> deptList = deptService.selectDeptList(new Dept());
        mmap.put("deptList", deptList);
        return prefix + "/problem";
    }

    /**
     * 查询问题墙列表
     */
    @RequiresPermissions("module:problem:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Problem problem)
    {
        startPage();
        List<Problem> list = problemService.selectProblemList(problem);
        return getDataTable(list);
    }

    /**
     * 导出问题墙列表
     */
    @RequiresPermissions("module:problem:export")
    @Log(title = "问题墙", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Problem problem)
    {
        List<Problem> list = problemService.selectProblemList(problem);
        ExcelUtil<Problem> util = new ExcelUtil<Problem>(Problem.class);
        return util.exportExcel(list, "problem");
    }

    /**
     * 新增问题墙
     */
    @GetMapping("/add")
    public String add(ModelMap mmap)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time= dateFormat.format(new Date());
        mmap.put("time",time);
        return prefix + "/add";
    }

    /**
     * 新增保存问题墙
     */
    @RequiresPermissions("module:problem:add")
    @Log(title = "问题墙", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Problem problem)
    {
        problem.setUserId(ShiroUtils.getSysUser().getUserId());
        problem.setProblemTime(DateUtils.getNowDate());
        return toAjax(problemService.insertProblem(problem));
    }

    /**
     * 修改问题墙
     */
    @GetMapping("/edit/{problemId}")
    public String edit(@PathVariable("problemId") Long problemId, ModelMap mmap)
    {
        Problem problem = problemService.selectProblemById(problemId);
        mmap.put("problem", problem);

        if(problem.getProblemVideo()!=null){
            String[] videos = problem.getProblemVideo().split(",");
            mmap.put("videos",videos);
        }
        
        return prefix + "/edit";
    }

    /**
     * 修改保存问题墙
     */
    @RequiresPermissions("module:problem:edit")
    @Log(title = "问题墙", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Problem problem)
    {
        return toAjax(problemService.updateProblem(problem));
    }

    /**
     * 删除问题墙
     */
    @RequiresPermissions("module:problem:remove")
    @Log(title = "问题墙", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(problemService.deleteProblemByIds(ids));
    }

    @GetMapping("/detail/{problemId}")
    public String detail(@PathVariable("problemId") Long problemId, ModelMap mmap)
    {
        Problem problem = problemService.selectProblemById(problemId);
        mmap.put("problem", problem);

        if(problem.getProblemVideo()!=null){
            String[] videos = problem.getProblemVideo().split(",");
            mmap.put("videos",videos);
        }

        return prefix + "/detail";
    }
}
